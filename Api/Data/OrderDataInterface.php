<?php

namespace Mbs\ApiWithObjectInMethodParameter\Api\Data;

use Magento\Catalog\Api\Data\ProductInterface;

interface OrderDataInterface
{
    const CUSTOMER_NAME = 'customer_name';

    const ORDER_TOTAL = 'order_total';

    /**
     * Set order total
     *
     * @param float $total
     */
    public function setOrderTotal($total);

    /**
     * Get order total
     *
     * @return float
     */
    public function getOrderTotal();

    /**
     * Set order customer name
     *
     * @param string $name
     */
    public function setCustomerName($name);

    /**
     * Get customer name
     *
     * @return string
     */
    public function getCustomerName();
}