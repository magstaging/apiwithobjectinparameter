<?php

namespace Mbs\ApiWithObjectInMethodParameter\Api;

use Mbs\ApiWithObjectInMethodParameter\Api\Data\OrderDataInterface;

interface Method1ForApiInterface
{
    /**
     * @param OrderDataInterface $order
     * @return string
     */
    public function getCustomerObjectForOrder(OrderDataInterface $order);

    /**
     * @return string
     */
    public function getSimpleCustomer();
}
