<?php

namespace Mbs\ApiWithObjectInMethodParameter\Model;

use Mbs\ApiWithObjectInMethodParameter\Api\Data\OrderDataInterface;
use Mbs\ApiWithObjectInMethodParameter\Api\Method1ForApiInterface;

class CustomerOrderRepository implements Method1ForApiInterface
{
    /**
     * @var \Mbs\ApiWithObjectInMethodParameter\Logger
     */
    private $logger;

    public function __construct(
        \Mbs\ApiWithObjectInMethodParameter\Logger $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param OrderDataInterface $order
     * @return string
     */
    public function getCustomerObjectForOrder(OrderDataInterface $order)
    {
        $this->logger->addLog('get customer api call');
        $this->logger->addLog($order);

        return sprintf('%s [total: %s]', $order->getCustomerName(), $order->getOrderTotal());
    }

    /**
     * @inheritDoc
     */
    public function getSimpleCustomer()
    {
        return 'test dummy customer';
    }
}
