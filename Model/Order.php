<?php

namespace Mbs\ApiWithObjectInMethodParameter\Model;

use Magento\Catalog\Model\AbstractModel;
use Mbs\ApiWithObjectInMethodParameter\Api\Data\OrderDataInterface;

class Order extends AbstractModel implements OrderDataInterface
{

    /**
     * @inheritDoc
     */
    public function setOrderTotal($total)
    {
        $this->setData(self::ORDER_TOTAL, $total);
    }

    /**
     * @inheritDoc
     */
    public function getOrderTotal()
    {
        return $this->_getData(self::ORDER_TOTAL);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerName($name)
    {
        $this->setData(self::CUSTOMER_NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerName()
    {
        return $this->_getData(self::CUSTOMER_NAME);
    }
}
