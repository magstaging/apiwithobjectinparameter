<?php

$order = ['order' => ['customer_name' => 'Herve', 'order_total' => '23']];
$url = 'http://magecertif.test/index.php/rest/V1/getcustomerorder/' . http_build_query($order);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt(
    $ch,
    CURLOPT_HTTPHEADER,
    [
        'Content-Type: application/json',
        'Authorization: Bearer or7ozb3e97x19nk97zylmhspul7niqi3'
    ]
);

//curl_setopt($ch,CURLOPT_POSTFIELDS, $str);

$result = curl_exec($ch);
curl_close($ch);

$result = json_decode($result);
print_r($result);

//// function to break onto: \Magento\Webapi\Model\ServiceMetadata::getServiceMetadata
/** @var  $proxy */
//$proxy = new SoapClient('http://magecertif.test/index.php/soap/default?wsdl&services=mbsApiWithObjectInMethodParameterMethod1ForApiV1');
//$result = $proxy->mbsApiWithObjectInMethodParameterMethod1ForApiV1GetCustomerObjectForOrder(["order"=> ['customer_name' => 'Herve', 'order_total' => '23']]);

//print_r($result);
