<?php

namespace Mbs\ApiWithObjectInMethodParameter\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetCustomerOrder extends Command
{
    public function __construct(
        $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('mbs:customer:search');
        $this->setDescription('Search customer via API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $token = 'or7ozb3e97x19nk97zylmhspul7niqi3';
        $httpHeaders = new \Zend\Http\Headers();
        $httpHeaders->addHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]);

        $request = new \Zend\Http\Request();
        $request->setHeaders($httpHeaders);
        $request->setUri('http://magecertif.test/index.php/rest/V1/customers/search');
        $request->setMethod(\Zend\Http\Request::METHOD_GET);

        $params = new \Zend\Stdlib\Parameters([
            'searchCriteria' => [
                'filterGroups' => [
                    0 => [
                        'filters' => [
                            0 => [
                                'field' => 'firstname',
                                'value' => '%ver%',
                                'condition_type' => 'like'
                            ],
                            1 => [
                                'field' => 'lastname',
                                'value' => '%Costello%',
                                'condition_type' => 'like'
                            ]
                        ]
                    ]
                ],
                 'current_page' => 1,
                 'page_size' => 10
             ],
         ]);

        $request->setQuery($params);

        $client = new \Zend\Http\Client();
        $options = [
            'adapter'   => 'Zend\Http\Client\Adapter\Curl',
            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
            'maxredirects' => 0,
            'timeout' => 30
        ];
        $client->setOptions($options);

        $response = $client->send($request);

        $output->writeln($response->getBody());

        $output->writeln('API is executed');
    }
}