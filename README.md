# README #

Returns customer's data for a give order. Whilst simple API calls will use string, integer in the input parameter, it is possible also to use an API
with an object as a parameter. We create a new ojbect in Magento that can store and retrieve 2 properties: customer_name and order_total. If
we pass one of these object's data to the API call, the API method processes these data and behaves like a normal API call thereafter.

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/ApiWithObjectInMethodParameter when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade